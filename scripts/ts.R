#TS
install.packages("car")
install.packages("xlsx")
install.packages("readxl")
install.packages("tidyverse")
install.packages("lubridate")
install.packages("timeSeries")
install.packages("forecast")
library(forecast)
library(timeSeries)
library(readxl)
library(car)
library(xlsx)
library(ggplot2)
library(dplyr)
library(tidyverse)
library(lubridate)

#11-Cargar datos de ventas.xlsx
ruta_archivo<-"ventas.xlsx"
ventas<-read_excel(ruta_archivo)
View(ventas)

#12-Convertir los datos de ventas a una serie de tiempo
ventas.ts<-ts(ventas,start = 1986, frequency = 12)

#13-Generar grafico de la serie de tiempo
plot(ventas.ts, type="o", col=1:2)

#14-Aplicar la función logaritmo a los datos y la descomposición de la serie de tiempo

#Aplicar el logaritmo
cierre_log<-log(ventas.ts[,1])
plot(cierre_log)

#Descomponer la serie de tiempo
cierre.stl<-stl(cierre_log,s.window = "period")
plot(cierre.stl)

#15-Aplicar función HoltWinters y comparar predicción con los datos originales
inf.hw<-HoltWinters(ventas.ts)
plot(inf.hw, col="blue", col.predicted="red", type="l")

#16-Aplicar forecast para realizar la predicción y generar la gráfica
inf.fore<-forecast(inf.hw, h=12)
plot(inf.fore)



















